import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonComponent} from './components/button/button.component';
import {UiIconComponent} from './components/ui-icon/ui-icon.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';

@NgModule({
  declarations: [
    ButtonComponent,
    UiIconComponent,
    DatepickerComponent,
  ],
  exports: [
    ButtonComponent,
    UiIconComponent,
    DatepickerComponent,
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule {
}
