import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewEncapsulation,
  Input,
} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,

})
export class ButtonComponent implements OnInit {
  @Input() public text: string;
  @Input() public className: string;
  @Input() public iconName: string;

  constructor() {
  }

  ngOnInit() {
  }
}
