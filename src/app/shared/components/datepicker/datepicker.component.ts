import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DatepickerComponent implements OnInit {
  @Input() value;

  constructor() {
  }

  ngOnInit() {
    console.log(new Date(this.value));
  }

  // public removeTime(): void {
  //   this.value -= 1;
  // }
  //
  // public addTime(): void {
  //   console.log(Date.parse(this.value) + 60);
  // }
}
