// CAR RENTAL EVENT
export {car} from './events/car-rental/car.icon';
export {bag} from './events/car-rental/bag.icon';
export {climat} from './events/car-rental/climat.icon';
export {passengers} from './events/car-rental/passengers.icon';

// HOTEL EVENT
export {hotel} from './events/hotel/hotel.icon';
export {star} from './events/hotel/star.icon';

// PLACE EVENT
export {place} from './events/place/place.icon';

// NEW EVENT
export {calendar} from './events/new-event/new.icon';
export {newcarrentals} from './events/new-event/new-carrental.icon';
export {newplace} from './events/new-event/new-place.icon';
export {newhotel} from './events/new-event/new-hotel.icon';

// BASE ICONS
export {arrow} from './base/arrow.icon';
export {carduration} from './base/car.icon';
export {deleteIcon} from './base/delete.icon';
export {edit} from './base/edit.icon';


