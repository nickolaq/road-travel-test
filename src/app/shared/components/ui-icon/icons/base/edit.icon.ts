export const edit = `
<svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
<g id="edit">
<mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="12" height="12">
<rect id="Mask" width="12" height="12" fill="white"/>
</mask>
<g mask="url(#mask0)">
<path id="editor-pencil-pen-edit-write-glyph" fill-rule="evenodd" clip-rule="evenodd" d="M4.30302 10.2536L9.60699 4.95003L6.77886 2.12194L1.47454 7.42515L4.30302 10.2536ZM0.414832 11.3151L0.83318 8.06796L3.6607 10.8954L0.414832 11.3151ZM10.2608 4.29693L11.361 3.19676C11.7286 2.82914 11.3235 2.42403 11.3235 2.42403L9.30528 0.405781C9.30528 0.405781 8.90017 0.000675981 8.53255 0.368297L7.43225 1.46841L10.2608 4.29693Z" fill="#00A2D3"/>
</g>
</g>
</svg>
`;
