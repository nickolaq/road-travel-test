export const calendar = `
  <svg width="28" height="30" viewBox="0 0 28 30" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect x="0.5" y="2.5" width="22" height="23" rx="0.5" stroke="#747E8A"/>
<rect x="0.5" y="2.5" width="22" height="5" rx="0.5" stroke="#747E8A"/>
<path d="M3.5 11C3.5 10.7239 3.72386 10.5 4 10.5H7.5V14.5H3.5V11Z" stroke="#747E8A"/>
<rect x="7.5" y="10.5" width="4" height="4" rx="0.5" stroke="#747E8A"/>
<rect x="11.5" y="10.5" width="4" height="4" rx="0.5" stroke="#747E8A"/>
<path d="M15.5 10.5H19C19.2761 10.5 19.5 10.7239 19.5 11V14.5H15.5V10.5Z" stroke="#747E8A"/>
<rect x="15.5" y="14.5" width="4" height="4" rx="0.5" stroke="#747E8A"/>
<rect x="3.5" y="14.5" width="4" height="4" rx="0.5" stroke="#747E8A"/>
<rect x="7.5" y="14.5" width="4" height="4" rx="0.5" stroke="#747E8A"/>
<rect x="11.5" y="14.5" width="4" height="4" rx="0.5" stroke="#747E8A"/>
<path d="M15.5 18.5H19.5V22C19.5 22.2761 19.2761 22.5 19 22.5H15.5V18.5Z" stroke="#747E8A"/>
<path d="M3.5 18.5H7.5V22.5H4C3.72386 22.5 3.5 22.2761 3.5 22V18.5Z" stroke="#747E8A"/>
<rect x="7.5" y="18.5" width="4" height="4" rx="0.5" stroke="#747E8A"/>
<rect x="11.5" y="18.5" width="4" height="4" rx="0.5" stroke="#747E8A"/>
<rect x="4.5" y="0.5" width="3" height="4" rx="0.5" fill="white" stroke="#747E8A"/>
<rect x="15.5" y="0.5" width="3" height="4" rx="0.5" fill="white" stroke="#747E8A"/>
<path d="M27.5 24C27.5 27.0376 25.0376 29.5 22 29.5C18.9624 29.5 16.5 27.0376 16.5 24C16.5 20.9624 18.9624 18.5 22 18.5C25.0376 18.5 27.5 20.9624 27.5 24Z" fill="white" stroke="#747E8A"/>
<path d="M22 26V22V24H20H24" stroke="#00A1D5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>

`;
