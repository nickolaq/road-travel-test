import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import * as icons from './icons/index';

@Component({
  selector: 'app-ui-icon',
  templateUrl: './ui-icon.component.html',
  styleUrls: ['./ui-icon.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UiIconComponent implements OnInit {

  @Input() protected iconName: string;

  public svg: SafeHtml;

  constructor(
    private sanitizer: DomSanitizer,
  ) {
    console.log(icons);
  }

  ngOnInit() {
    this.svg = this.sanitizer.bypassSecurityTrustHtml(icons[this.iconName]);
  }

}
