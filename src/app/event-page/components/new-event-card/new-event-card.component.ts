import {Component, HostBinding, HostListener, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-new-event-card',
  templateUrl: './new-event-card.component.html',
  styleUrls: ['./new-event-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NewEventCardComponent implements OnInit {
  public eventsType: string[] = ['place', 'hotel', 'car rentals'];
  public isShow = false;
  @HostBinding('class') class = 'new-event-wrp';

  constructor() {
  }

  ngOnInit() {
  }

  public showMessage(type: string): void {
    alert(type.toUpperCase());
  }

  @HostListener('click', ['$event'])
  protected openDropdown($event): void {
    this.isShow = !this.isShow;
    $event.stopPropagation();
  }

  @HostListener('document:click', ['$event'])
  protected clickOutside($event) {
    this.isShow = false;
    $event.stopPropagation();
  }
}
