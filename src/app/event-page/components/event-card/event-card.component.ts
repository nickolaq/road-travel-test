import {
  Component,
  HostBinding,
  Input,
  Output,
  OnInit,
  ViewEncapsulation,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class EventCardComponent implements OnInit {
  @Input() public event;
  @Input() public eventsCard;
  @Input() public index;
  @Output() public emitDeleteCard = new EventEmitter();
  @HostBinding('class') class = 'card-wrp';

  public isEdit;

  constructor() {
  }

  ngOnInit() {
  }

  public enableEditMode(): void {
    this.isEdit = true;
  }

  public emitSaveCard(value): void {
    this.isEdit = value;
  }

  public emitCancelEdits(value): void {
    this.isEdit = value;
  }

  public deleteEvent(index: number): void {
    this.emitDeleteCard.emit(index);
  }
}
