import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-meta-car-info',
  templateUrl: './meta-car-info.component.html',
  styleUrls: ['./meta-car-info.component.scss']
})
export class MetaCarInfoComponent implements OnInit {
  @Input() eventCard;

  constructor() {
  }

  ngOnInit() {
  }

}
