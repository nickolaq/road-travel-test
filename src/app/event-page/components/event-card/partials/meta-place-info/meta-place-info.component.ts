import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-meta-place-info',
  templateUrl: './meta-place-info.component.html',
  styleUrls: ['./meta-place-info.component.scss']
})
export class MetaPlaceInfoComponent implements OnInit {
  @Input() eventCard;

  constructor() {
  }

  ngOnInit() {
  }

}
