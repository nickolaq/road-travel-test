import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-meta-hotel-info',
  templateUrl: './meta-hotel-info.component.html',
  styleUrls: ['./meta-hotel-info.component.scss']
})
export class MetaHotelInfoComponent implements OnInit {
  @Input() eventCard;

  constructor() {
  }

  ngOnInit() {
  }

}
