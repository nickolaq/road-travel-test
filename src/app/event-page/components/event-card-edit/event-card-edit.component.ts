import {
  Component,
  OnInit,
  Input,
  Output,
  HostBinding,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'app-event-card-edit',
  templateUrl: './event-card-edit.component.html',
  styleUrls: ['./event-card-edit.component.scss']
})
export class EventCardEditComponent implements OnInit {
  @Input() public eventCard;
  @Output() public emitSaveCard = new EventEmitter<boolean>();
  @Output() public emitCancelEdits = new EventEmitter<boolean>();
  @HostBinding('class') class = 'card-edit-wrp';

  constructor() {
  }

  ngOnInit() {
  }

  public saveCard(value: boolean): void {
    this.emitSaveCard.emit(value);
  }

  public cancelCard(value: boolean): void {
    this.emitCancelEdits.emit(value);
  }

}
