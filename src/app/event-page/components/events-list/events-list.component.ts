import {Component, HostBinding, OnInit, ViewEncapsulation} from '@angular/core';
import {EventPageService} from 'src/app/event-page/services/event-page.service';
import {EventCardModel} from './model/event-card.model';
import {IExampleData} from '../../../interfaces';

interface IEventCardModel extends IExampleData {
  duration: number;
}

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class EventsListComponent implements OnInit {
  public eventsCard: any[];
  @HostBinding('class') class = 'events-list-wrp';

  constructor(
    protected eventPageService: EventPageService,
  ) {
  }

  ngOnInit() {
    this.eventsCard = this.eventPageService.eventsData.map((el: IExampleData) => new EventCardModel(el));
  }

  public onDeleteCard(index: number): void {
    this.eventsCard.splice(index, 1);
  }
}
