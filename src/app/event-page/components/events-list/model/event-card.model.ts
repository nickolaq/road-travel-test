import {IExampleData} from "src/app/interfaces/index";

export class EventCardModel {
  public duration = 40;

  constructor(
    eventCard: IExampleData,
  ) {
    Object.assign(this, eventCard);
  }
}
