import {Injectable} from '@angular/core';
import {exampleData} from 'src/app/mock-data/data';
import {IExampleData} from "../../interfaces";

export interface IEventService {
  eventsData: any;
}

@Injectable()
export class EventPageService implements IEventService {

  public get eventsData(): IExampleData[] {
    return this.exampleData;
  }

  private exampleData: IExampleData[] = exampleData;

  constructor() {
  }
}
