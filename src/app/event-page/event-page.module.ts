import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EventPageComponent} from './event-page.component';
import {EventCardComponent} from './components/event-card/event-card.component';
import {NewEventCardComponent} from './components/new-event-card/new-event-card.component';
import {SharedModule} from '../shared/shared.module';
import {EventsListComponent} from './components/events-list/events-list.component';
import {EventPageService} from './services/event-page.service';
import {MapComponent} from './components/map/map.component';
import { MetaHotelInfoComponent } from './components/event-card/partials/meta-hotel-info/meta-hotel-info.component';
import { MetaPlaceInfoComponent } from './components/event-card/partials/meta-place-info/meta-place-info.component';
import { MetaCarInfoComponent } from './components/event-card/partials/meta-car-info/meta-car-info.component';
import { EventCardEditComponent } from './components/event-card-edit/event-card-edit.component';


@NgModule({
  declarations: [
    EventPageComponent,
    EventsListComponent,
    EventCardComponent,
    NewEventCardComponent,
    MapComponent,
    MetaHotelInfoComponent,
    MetaPlaceInfoComponent,
    MetaCarInfoComponent,
    EventCardEditComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
  ],
  providers: [
    EventPageService,
  ],
  exports: [
    EventPageComponent,
  ]
})
export class EventPageModule {
}
