export const exampleData = [
  {
    type: 'car-rental',
    title: 'Mercedes Benz',
    st: '2020-10-02T20:00',
    vehicle: {
      type: 'E Class',
      gearbox: 'A',
      ac: true,
    },
  },

  {
    type: 'hotel',
    title: 'Relais du Silence Aux Vieux Remparts',
    st: '2020-10-02T20:00',
    hotel: {
      rating: '8.5',
      starRating: 5,
      category: 'Premium Hotel',
    },
  },


  {
    type: 'place',
    title: 'Hyde-park',
    st: '2020-10-02T20:00',
    place: {
      category: 'Park',
    },
  },
];
