import {IEventService} from '../event-page/services/event-page.service';

export interface IExampleData {
  type: string;
  title: string;
  st: string;
  vehicle?: IVehicle;
  hotel?: IHotel;
  place?: IPlace;
}

export interface IVehicle {
  type: string;
  gearbox: string;
  ac: boolean;
}

export interface IHotel {
  rating: string;
  starRating: number;
  category: string;
}

export interface IPlace {
  category: string;
}
